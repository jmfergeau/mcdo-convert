"use strict";
const prices = {
    bigmac: 5.55,
    frite: 3.55,
};
const prixBM = document.getElementById("prixBM"), prixF = document.getElementById("prixF"), convert = document.getElementById("convert"), inputEuro = document.getElementById("inputEuro"), errorMsg = document.getElementById("errorMsg"), resultHTML = document.getElementById("result");
prixBM.innerHTML = prices.bigmac.toString();
prixF.innerHTML = prices.frite.toString();
function mcdoConvert(price) {
    if (price < 0.5 || price >= 9999999999) {
        errorMsg.classList.remove("d-none");
        return "";
    }
    else {
        errorMsg.classList.add("d-none");
    }
    let operation1 = price / prices.bigmac, operation2 = price % prices.frite;
    console.log("Bigmacs: " + operation1.toString() + "\nFrites: " + operation2.toString());
    operation1 = parseInt(operation1.toFixed());
    operation2 = parseInt(operation2.toFixed());
    let result = price + " € font<br/>";
    console.log("Bigmacs arrondis: " +
        operation1.toString() +
        "\nFrites arrondis: " +
        operation2.toString());
    if (operation1 > 1) {
        if (operation1 == 1) {
            result = result + operation1.toFixed() + " Big Mac&trade; 🍔";
        }
        else if (operation1 > 1) {
            result = result + operation1.toFixed() + " Big Macs&trade; 🍔";
        }
    }
    if (operation2 != 0) {
        if (operation1 > 1)
            result = result + "<br/>et " + operation2.toFixed();
        else
            result = result + operation2.toFixed();
        if (operation2 == 1) {
            result = result + " frite 🍟";
        }
        else {
            result = result + " frites 🍟";
        }
    }
    resultHTML.innerHTML = result + " !";
    return;
}
convert.addEventListener("click", function (e) {
    e.preventDefault();
    mcdoConvert(parseFloat(inputEuro.value));
});
