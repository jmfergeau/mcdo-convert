// Prix basés sur ceux d'un big mac et d'une frite moyenne en février 2023
type Prices = { bigmac: number; frite: number };
const prices: Prices = {
  bigmac: 5.55,
  frite: 3.55,
};

// Déclarations des éléments du DOM
const prixBM = document.getElementById("prixBM") as HTMLTextAreaElement, // Prix du BigMac dans la section "techniquement"
  prixF = document.getElementById("prixF") as HTMLTextAreaElement, // Prix des frites dans la section "techniquement"
  convert = document.getElementById("convert") as HTMLButtonElement, // Bouton qui active la conversion avec un clic
  inputEuro = document.getElementById("inputEuro") as HTMLInputElement, // Le champ de la somme en euro
  errorMsg = document.getElementById("errorMsg") as HTMLDivElement, // Bloc de message d'erreur
  resultHTML = document.getElementById("result") as HTMLParagraphElement; // Bloc qui affiche le résultat

// Affiche les prix originaux dans la section "Techniquement..."
prixBM.innerHTML = prices.bigmac.toString();
prixF.innerHTML = prices.frite.toString();

function mcdoConvert(price: number) {
  // Le garde-fou. Il ramène une erreur si le chiffre est trop élevé, trop bas ou pas un chiffre
  // On note qu'une valeur supérieure à 9999999998 est considéré comme une erreur.
  // C'est parce que cette valeur ou un chiffre supérieur fait apparaitre un résultat buggué...
  // Probablement une limite de JS... La limite minimale est aussi limité pour ne pas retourner
  // un résultat vide.
  if (price < 0.5 || price >= 9999999999) {
    // Affiche le bloc d'erreur et arrête le script
    errorMsg.classList.remove("d-none");
    return "";
  } else {
    // Si l'utilisateur a mis un bon chiffre après une erreur, on cache le message.
    errorMsg.classList.add("d-none");
  }

  // L'opération. On divise pour les bigmacs et on prend le modulo pour les frites, puis on arrondit avec toFixed.
  let operation1: number = price / prices.bigmac,
    operation2: number = price % prices.frite;

  console.log(
    "Bigmacs: " + operation1.toString() + "\nFrites: " + operation2.toString()
  );

  // Arrondissement en entier...
  // Ne me demandez pas pourquoi il faut reparser en Int des variables qui sont censé être des numbers... WTF...
  operation1 = parseInt(operation1.toFixed());
  operation2 = parseInt(operation2.toFixed());

  // On commence à écrire le résultat. On rapporte d'abord la somme donnée
  let result: string = price + " € font<br/>";

  console.log(
    "Bigmacs arrondis: " +
      operation1.toString() +
      "\nFrites arrondis: " +
      operation2.toString()
  );

  // Ajoute les bigmacs. Biffe cette partie là si aucun bigmac.
  if (operation1 > 1) {
    // Mise au pluriel des bigmacs
    if (operation1 == 1) {
      result = result + operation1.toFixed() + " Big Mac&trade; 🍔";
    } else if (operation1 > 1) {
      result = result + operation1.toFixed() + " Big Macs&trade; 🍔";
    }
  }

  // Rajout des frites s'il y en a et mise au pluriel s'il faut.
  if (operation2 != 0) {
    if (operation1 > 1) result = result + "<br/>et " + operation2.toFixed();
    else result = result + operation2.toFixed();
    if (operation2 == 1) {
      result = result + " frite 🍟";
    } else {
      result = result + " frites 🍟";
    }
  }

  // Renvoi de la réponse avec un ! pour conclure la réponse
  resultHTML.innerHTML = result + " !";
  return;
}

// Events listeners
convert.addEventListener("click", function (e) {
  e.preventDefault();
  mcdoConvert(parseFloat(inputEuro.value));
});
